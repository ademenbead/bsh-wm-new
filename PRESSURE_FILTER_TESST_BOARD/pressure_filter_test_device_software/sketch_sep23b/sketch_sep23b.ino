#include "Arduino.h"

#include "SPI.h"
#include "tft.h"
#include "mainmenu.h"
#include "capacityscreen.h"
#include "prescreen.h"
#include <fonts/Times_New_Roman.h>
#include "atlasbitmap.h"


const int pulsePin = 15;
const int motorPin = 5;
int buzzerPin = 12;
int pulseHigh; // Integer variable to capture High time of the incoming pulse
int pulseLow; // Integer variable to capture Low time of the incoming pulse
float pulseTotal; // Float variable to capture Total time of the incoming pulse
float frequency; // Calculated Frequency

MainMenu mainmenu;
CapacityScreen c_screen;
PreScreen p_screen;

String active_screen = "mainmenu";
TFT tft(1); //0=ILI9341, 1= HX8347D



#define ACTIVE LOW

const int okButtonPin = 34;
int okButtonState = 0;

const int cancelButtonPin = 35;
int cancelButtonState = 0;

const int leftButtonPin = 27;
int leftButtonState = 0;

const int rightButtonPin = 13;
int rightButtonState = 0;

const int upButtonPin = 14;
int upButtonState = 0;

const int downButtonPin = 26;
int downButtonState = 0;

void setup() {
  
  Serial.begin(115200);
  tft.begin();
  
  tft.fillScreen(TFT_WHITE);
  
  tft.setRotation(1);
  
  for(uint16_t i = 0; i < DRAGON_HEIGHT; i++){
    
     for(uint16_t j = 0; j < DRAGON_WIDTH; j++){
        if(atlasbitmap[j+i*DRAGON_WIDTH] != 65535){
          tft.drawPixel(i,j,atlasbitmap[j+i*DRAGON_WIDTH]);
        }
     }
  }


  delay(4000);
  
  tft.fillScreen(TFT_WHITE);
  mainmenu.init();

  

  

  
  /*
  for(uint16_t i = 0; i < DRAGON_HEIGHT; i++){
     for(uint16_t j = 0; j < DRAGON_WIDTH; j++){
        tft.writePixel(i,j,atlasbitmap[j+i*DRAGON_WIDTH]);
     }
  }
  */
  /*pinMode(motorPin, OUTPUT);
  digitalWrite(motorPin,HIGH);
  */
  //ledcWrite(0, 210  );
  
  //Fiziksek Button init neden pull up oldu bilmiyorum böyle deneyince calıstı
  //normal halde high geliyor basınca 0 oluyor
  pinMode(okButtonPin, INPUT_PULLUP);
  pinMode(cancelButtonPin, INPUT_PULLUP);
  pinMode(leftButtonPin, INPUT_PULLUP);
  pinMode(rightButtonPin, INPUT_PULLUP);
  pinMode(upButtonPin, INPUT_PULLUP);
  pinMode(downButtonPin, INPUT_PULLUP);
  pinMode(buzzerPin, OUTPUT);
  
  //tft.drawRGBBitmap((int16_t)0, (int16_t)0, (uint16_t*)atlasbitmap, (int16_t)DRAGON_HEIGHT, (int16_t)DRAGON_WIDTH);
  
  
}
void loop() {

  
  //handleSerial();
   if (Serial.available() > 0) {
        //char ch = Serial.read();
        // show the byte on serial monitor
        int val = Serial.parseInt();
        //Serial.println(val);
        ledcWrite(0, val);
    }
  buttonClickHandle(); 

  if(active_screen == "prescreen"){
    p_screen.RemoteLooper();
  }

  //Serial.println(frequency);
    
}
void Buzzer(int delays){
  digitalWrite(buzzerPin,HIGH);
  delay(delays);
  digitalWrite(buzzerPin,LOW);  
}
void buttonClickHandle(){
  

   //ok button
   if(digitalRead(okButtonPin) == ACTIVE){
      if(okButtonState == 0){
          okButtonState = 1;
          //Serial.println("enter");
          buttonPressed("enter");
          delay(200);
      }
   }else{
      okButtonState = 0;
   }
    //Cancel button
   if(digitalRead(cancelButtonPin) == ACTIVE){
      if(cancelButtonState == 0){
          cancelButtonState = 1;
          //Serial.println("cancel");
          buttonPressed("cancel");
          delay(200);
      }
   }else{
      cancelButtonState = 0;
   }

   //Left button
   if(digitalRead(leftButtonPin) == ACTIVE){
      if(leftButtonState == 0){
          leftButtonState = 1;
          //Serial.println("left");
          buttonPressed("left");
          delay(200);
      }
   }else{
      leftButtonState = 0;
   }

   //Right button
   if(digitalRead(rightButtonPin) == ACTIVE){
      if(rightButtonState == 0){
          rightButtonState = 1;
          //Serial.println("right");
          buttonPressed("right");
          delay(200);
      }
   }else{
      rightButtonState = 0;
   }

  //Down button
   if(digitalRead(upButtonPin) == ACTIVE){
      if(upButtonState == 0){
          upButtonState = 1;
          //Serial.println("up");
          buttonPressed("up");
          delay(200);
      }
   }else{
      upButtonState = 0;
   }
  

   //Down button
   if(digitalRead(downButtonPin) == ACTIVE){
      if(downButtonState == 0){
          downButtonState = 1;
          //Serial.println("down");
          buttonPressed("down");
          delay(200);
      }
   }else{
      downButtonState = 0;
   }

   
}
void buttonPressed(String button){
  Buzzer(140);
  String go_screen = "";
  if(active_screen == "mainmenu"){
      go_screen = mainmenu.ButtonPressed(button);
  }
  if(active_screen == "capacityscreen"){
      go_screen = c_screen.ButtonPressed(button);
  }

   if(active_screen == "prescreen"){
      go_screen = p_screen.ButtonPressed(button);
  }
  
  if(go_screen != active_screen){
    if(go_screen == "capacityscreen"){
        c_screen.init();
        active_screen = go_screen;
    }
    if(go_screen == "mainmenu"){
        mainmenu.init();
        active_screen = go_screen;
    }
    if(go_screen == "prescreen"){
        p_screen.init();
        active_screen = go_screen;
    }
  }else{
    
  }

  //Serial.println(go_screen);
}

void handleSerial() {
 while (Serial.available() > 0) {
   char incomingCharacter = Serial.read();
   //Serial.println(incomingCharacter);
   switch (incomingCharacter) {
     case '2':
      //Serial.println("asagi");
      //buttonPressed("down");
      break;
 
     case '4':
      //Serial.println("sola");
      //buttonPressed("left");
      break;

     case '6':
      //Serial.println("saga");
      //buttonPressed("right");
      break;

     case '8':
      //Serial.println("yukari");
      //buttonPressed("up");
      break;

     case '1':
      //Serial.println("cancel");
      //buttonPressed("cancel");
      break;

     case '3':
      //Serial.println("enter");
      //buttonPressed("enter");
      break;
    }
 }
}
