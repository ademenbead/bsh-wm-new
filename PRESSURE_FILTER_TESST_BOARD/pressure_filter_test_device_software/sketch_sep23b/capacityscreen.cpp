#include "capacityscreen.h"
#include "Arduino.h"
#include "SPI.h"
#include "tft.h" 
#include <fonts/Times_New_Roman.h>

#define analogPin      33          
#define chargePin      32     
#define dischargePin   25        
#define resistorValue  10000.0F   

unsigned long startTime;
unsigned long elapsedTime;
float microFarads;                
float nanoFarads;

CapacityScreen::CapacityScreen() {
  _tft.begin();
  _tft.setFont(Times_New_Roman15x14);
}
void CapacityScreen::init() {
  _tft.setRotation(1);
  _tft.fillScreen(TFT_WHITE);

  
  
  pinMode(chargePin, OUTPUT);
  pinMode(analogPin, INPUT);     
  digitalWrite(chargePin, LOW);  

  
  StatusText("Ready");
  
  DrawMenu(1);
  
}

void CapacityScreen::DrawMenu(int selected){

    if(selected == 1) FirstElement(TFT_ORANGE); else FirstElement(TFT_BLACK);
   
    
}
void CapacityScreen::StatusText(String text){
  ClearActionArea();
  _tft.setTextColor(TFT_BLACK);
  _tft.setTextSize(6);
  _tft.setCursor(34,34);
  _tft.println(text);
}
void CapacityScreen::StatusTextLineTwo(String text){
  _tft.setTextColor(TFT_BLACK);
  _tft.setTextSize(6);
  _tft.setCursor(34,74);
  _tft.println(text);
}
void CapacityScreen::ClearActionArea(){
  _tft.fillRect(0,0,320,149,TFT_WHITE);
}

void CapacityScreen::FirstElement(int bordercolor){
  
  //x,y,width,height,color
  
  _tft.drawRect(30,150,260,60,bordercolor);
  _tft.drawRect(31,151,258,58,bordercolor);
  _tft.drawRect(32,152,256,56,bordercolor);
  _tft.drawRect(33,153,254,54,bordercolor);

  _tft.setTextColor(bordercolor);
  _tft.setTextSize(6);
  _tft.setCursor(97,162);
  _tft.println("Start Test");
}

void CapacityScreen::ChargeMode(int val){
  digitalWrite(chargePin, val);  // set chargePin HIGH and capacitor charging
}
void CapacityScreen::DischargeMode(int val){
  if(val == 1){
    pinMode(dischargePin, OUTPUT);            // set discharge pin to output 
    digitalWrite(dischargePin, LOW);  
  }else{
    pinMode(dischargePin, INPUT);
  }
}
void CapacityScreen::Test(){
  float totalcap = 0.0;
  int totalloop = 0;
  for (int i=0; i < 8; i++){
    totalloop++;
    if(analogRead(analogPin) > 0){
      //StatusText("Capacity discharging..");
      DischargeMode(1);   
      int disstartTime = millis();     
      while(analogRead(analogPin) > 0){   
        int msfarkdis = millis() - disstartTime;
        if(msfarkdis > 10000){
          StatusText("Fault-#02");
          ChargeMode(0);             
          DischargeMode(0);
          return;
        }      
      }
      DischargeMode(0); 
      delay(500);
        
    }
    //StatusText("Charging..");
    ChargeMode(1);
    DischargeMode(0);
    startTime = millis();
  
    while(analogRead(analogPin) < 2588){       // 647 is 63.2% of 1023, which corresponds to full-scale voltage 
      int msfark = millis() - startTime;
      if(msfark > 10000){
        ChargeMode(0);             
        DischargeMode(0);
        StatusText("Is Not Charged");
        return;
      }
    }
  
    elapsedTime= millis() - startTime;
   // convert milliseconds to seconds ( 10^-3 ) and Farads to microFarads ( 10^6 ),  net 10^3 (1000)  
    microFarads = ((float)elapsedTime / resistorValue) * 1000;   
    microFarads = (float)(microFarads * 0.71);
    totalcap = (float)totalcap + (float)microFarads;


    /* dicharge the capacitor  */
    ChargeMode(0);             
    DischargeMode(1);        
    while(analogRead(analogPin) > 0){         // wait until capacitor is completely discharged
      
    }
  
    DischargeMode(0);   
  
    //Serial.println(microFarads);
    //Serial.println(totalcap);

    String statustext;
    statustext = F("Sampling:");
    statustext+= String(totalloop);
    statustext+= F("/8");
    StatusText(statustext);
    

  }
  
  
  
  microFarads = totalcap / totalloop;
  String statustext;
  statustext = F("Cap Value: ");
  statustext+= String(microFarads,2);
  statustext+= F("uF");
  StatusText(statustext);

  String DifferenceCap;
  DifferenceCap = F("Difference: %");
  DifferenceCap+= String(PercentDif(0.47,microFarads),2);

  StatusTextLineTwo(DifferenceCap);

  
   

}
float CapacityScreen::PercentDif(float readlval,float calculated){
   float diff = 100 * (calculated-readlval)/calculated;
    return diff;  
}
String CapacityScreen::ButtonPressed(String button){
  
  if(button=="enter"){
      //Serial.println("Capacity Test Enter");
      Test();
      return "capacityscreen";
  }
  if(button=="cancel"){
      //Serial.println("Capacity Test exit");
      //Test();
      ChargeMode(0);
      DischargeMode(0);
      return "mainmenu";
  }

  return "capacityscreen";
  
}
