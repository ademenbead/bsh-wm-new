#include "mainmenu.h"
#include "Arduino.h"
#include "SPI.h"
#include "tft.h" 
#include <fonts/Times_New_Roman.h>
MainMenu::MainMenu() {
  _tft.begin();
  _tft.setFont(Times_New_Roman15x14);
}
void MainMenu::init() {
  
  _tft.fillScreen(TFT_WHITE);
  _tft.setRotation(1);
  
  DrawMenu(customitem);

}

void MainMenu::DrawMenu(int selected){

    if(selected == 1) FirstElement(TFT_ORANGE); else FirstElement(TFT_BLACK);
    if(selected == 2) SecondElement(TFT_ORANGE); else SecondElement(TFT_BLACK);
    
}

void MainMenu::FirstElement(int bordercolor){
  
  //x,y,width,height,color
  
  _tft.drawRect(30,30,260,60,bordercolor);
  _tft.drawRect(31,31,258,58,bordercolor);
  _tft.drawRect(32,32,256,56,bordercolor);
  _tft.drawRect(33,33,254,54,bordercolor);

  _tft.setTextColor(bordercolor);
  _tft.setTextSize(6);
  _tft.setCursor(75,43);
  _tft.println("Pressure Test");
}
String MainMenu::ButtonPressed(String button){
  if(button=="up"){
     if(customitem > 1){
        customitem = --customitem;
        DrawMenu(customitem);
        return "mainmenu";
      }
      if(customitem == 1){
        customitem = itemmax;
        DrawMenu(customitem);
        return "mainmenu";
      }
      return "mainmenu";
  }

  if(button=="down"){
     if(customitem < itemmax){
        customitem = ++customitem;
        DrawMenu(customitem);
        return "mainmenu";
      }
      if(customitem == itemmax){
        customitem = 1;
        DrawMenu(customitem);
        return "mainmenu";
      }
      return "mainmenu";
  }
  if(button=="enter"){
     
     if(customitem == 1) return "prescreen";
     if(customitem == 2) return "capacityscreen";
  }

  return "mainmenu";
  
  //Serial.println(customitem,DEC);
}
void MainMenu::SecondElement(int bordercolor){

  //x,y,width,height,color
  
  _tft.drawRect(30,150,260,60,bordercolor);
  _tft.drawRect(31,151,258,58,bordercolor);
  _tft.drawRect(32,152,256,56,bordercolor);
  _tft.drawRect(33,153,254,54,bordercolor);
  
  _tft.setTextColor(bordercolor);
  _tft.setTextSize(6);
  _tft.setCursor(90,162);
  _tft.println("Filter Test");
  
}
