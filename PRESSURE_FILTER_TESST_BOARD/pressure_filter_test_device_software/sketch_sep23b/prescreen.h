#ifndef PreScreen_h
#define PreScreen_h
#include "SPI.h"
#include "prescreen.h"
#include "tft.h"
class PreScreen{
  #define totalStepCount 13
  public:
      PreScreen();
      void init();
      void DrawMenu(int);
      void AutoTest(int);
      void ManuelTest(int);
      String ButtonPressed(String);
      void DrawResultArea();
      void RemoteLooper();
      void AutoTestStart();
      String PercentDiffCalculate(float,float);
      
      void SetCheckedPoints(float,float);
      int GetPointIndex(float);
      void AutoTestStop();
      void ResetStepArray();
      void AutoTestStop(int);
      int CalcPa(float);
      void FastRefClear();
      void FastPlugClear();
      void Buzzer(int);
      void DrawTestboxes();
      void DrawTestedBoxes();
      void ShowTestResult();
      
  private:
      TFT _tft;
      int customitem = 1;
      int pulsePin = 4;
      
      int plugpulsePin = 15;
      
      int motorPin = 5;
      int buzzerPin = 12;

      int autotestrunning = 0;
      int teststarttime = 0;
      int screenrefreshtime = 0;
      int firststarttest = 0;
      int startact = 0;
      const int startval = 12405;
      const int artisoran = 10;
      int pulseHigh; // Integer variable to capture High time of the incoming pulse
      int pulseLow; // Integer variable to capture Low time of the incoming pulse
      float pulseTotal; // Float variable to capture Total time of the incoming pulse
      float reffreq; // Calculated Frequency

      int plugpulseHigh; // Integer variable to capture High time of the incoming pulse
      int plugpulseLow; // Integer variable to capture Low time of the incoming pulse
      float plugpulseTotal; // Float variable to capture Total time of the incoming pulse
      float plugreffreq; // Calculated Frequency

      float oldRefHz = 0.000;
      int oldRefPa = 0;

      float oldPlugHz = 0.000;
      int oldPlugPa = 0;

      String oldDiff = "";
      
      float diffHz = 0;
      //Test result if index
      //0 not checked 1 - success 2 - fail
     
      int currentchecked = 0;
      
      
      int TestedSteps[totalStepCount] = {0,0,0,0,0,0,0,0,0,0,0,0,0};
      float TestedStepPercents[totalStepCount] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
      char *TestedStepsStr[totalStepCount]={
        " 1- 609.180 - 625.000",
        " 2- 609.000 - 609.180",
        " 3- 593.500 - 609.000",
        " 4- 589.400 - 593.500",
        " 5- 578.000 - 589.400",
        " 6- 563.700 - 578.000",
        " 7- 563.490 - 563.700",
        " 8- 556.300 - 563.490",
        " 9- 549.800 - 556.300",
        "10- 536.800 - 549.800",
        "11- 524.900 - 536.800",
        "12- 513.600 - 524.900",
        "13- 503.100 - 513.600"
      };
      int percenttolerancemax = 3;
      int percenttolerancemin = -3;
      int isResultShowing = 0;

      int RefAttach=1;
      int PlugAttach=1;
      
};
  
#endif
