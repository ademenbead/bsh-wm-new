#ifndef MainMenu_h
#define MainMenu_h
#include "SPI.h"
#include "mainmenu.h"
#include "tft.h"
class MainMenu{
  public:
      MainMenu();
      void init();
      void DrawMenu(int);
      void FirstElement(int);
      void SecondElement(int);
      String ButtonPressed(String);
  private:
      TFT _tft;
      int itemmax = 2;
      int customitem = 1;
      
      
};
  
#endif

