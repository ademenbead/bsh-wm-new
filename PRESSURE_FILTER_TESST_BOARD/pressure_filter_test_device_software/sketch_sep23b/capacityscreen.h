#ifndef CapacityScreen_h
#define CapacityScreen_h
#include "SPI.h"
#include "capacityscreen.h"
#include "tft.h"
class CapacityScreen{
  public:
      CapacityScreen();
      void init();
      void DrawMenu(int);
      void FirstElement(int);
      String ButtonPressed(String);
      void Test();
      void ClearActionArea();
      void StatusText(String);
      void StatusTextLineTwo(String);
      void ChargeMode(int);
      void DischargeMode(int);
      float PercentDif(float,float);
  private:
      TFT _tft;
      int itemmax = 1;
      int customitem = 1;

      
      
};
  
#endif
