#include "prescreen.h"
#include "Arduino.h"
#include "SPI.h"
#include "tft.h"
#include <fonts/Times_New_Roman.h>
PreScreen::PreScreen() {
  _tft.begin();
  _tft.setFont(Times_New_Roman15x14);
}
void PreScreen::init() {
  _tft.setRotation(1);
  _tft.fillScreen(TFT_WHITE);
  
  ledcSetup(0, 5000, 16);
  ledcAttachPin(motorPin, 0);
  
  pinMode(pulsePin,INPUT);
  DrawMenu(customitem);
  DrawResultArea();
  pinMode(buzzerPin, OUTPUT);
  RefAttach=1;
  PlugAttach=1;
  
}
void PreScreen::Buzzer(int delays){
  digitalWrite(buzzerPin,HIGH);
  delay(delays);
  digitalWrite(buzzerPin,LOW);  
}
void PreScreen::RemoteLooper(){
  if(isResultShowing == 1){
    return;  
  }
  pulseHigh = pulseIn(pulsePin,HIGH);
  pulseLow = pulseIn(pulsePin,LOW);
  pulseTotal = pulseHigh + pulseLow; // Time period of the pulse in microseconds

  
  plugpulseHigh = pulseIn(plugpulsePin,HIGH);
  plugpulseLow = pulseIn(plugpulsePin,LOW);
  plugpulseTotal = plugpulseHigh + plugpulseLow; // Time period of the pulse in microseconds

  
  //Takılan sensörün olmaması durumunda aşağıdaki durum işletilir.
  //###############################################
  
  if(plugpulseTotal < 500){
    if(PlugAttach == 1){
      PlugAttach = 0;
      //Serial.println("no plugged sensor");
      _tft.fillRect(188,52,150,60,TFT_WHITE);
      _tft.fillRect(115,109,100,45,TFT_WHITE);
      _tft.setTextColor(TFT_RED);
      _tft.setTextSize(4);
    
      //plug yerin hz yazısı
      _tft.setCursor(188,52);
      _tft.println("No Sensor");
      //plug yerin pa yazısı
      _tft.setCursor(188,82);
      _tft.println("Detected!");
      
    }else{
      
      
    
    }
  
  }else{
    if(PlugAttach == 0){
      _tft.fillRect(188,52,150,80,TFT_WHITE);
      PlugAttach = 1;
      //Serial.println("plug  cizdirdi");
      RefAttach=1;
      DrawResultArea();
    }
    
  }
  //###############################################

  //Referans sensörün olmaması durumunda aşağıdaki durum işletilir.
  //###############################################
  
  if(pulseTotal < 500){
    if(RefAttach == 1){
      RefAttach = 0;
      //Serial.println("no plugged reference");
      _tft.fillRect(0,52,150,60,TFT_WHITE);
      _tft.fillRect(115,109,100,45,TFT_WHITE);
      _tft.setTextColor(TFT_RED);
      _tft.setTextSize(4);
    
      //plug yerin hz yazısı
      _tft.setCursor(28,52);
      _tft.println("No Sensor");
      //plug yerin pa yazısı
      _tft.setCursor(28,82);
      _tft.println("Detected!");
      
    }else{
      
      
    
    }
  
  }else{
    if(RefAttach == 0){
      _tft.fillRect(188,52,150,80,TFT_WHITE);
      RefAttach = 1;

      //Serial.println("ref  cizdirdi");
      //Serial.println(pulseTotal);
      PlugAttach = 1;
      DrawResultArea();
    }
    
  }
  //###############################################
  if(plugpulseTotal < 500 || pulseTotal < 500){
    AutoTestStop(0);
    return;  
  }
  
  reffreq=1000000/pulseTotal; // Frequency in Hertz (Hz)
  
  
  plugreffreq=1000000/plugpulseTotal; // Frequency in Hertz (Hz)
  
  //Serial.println(reffreq,3);

  
  int fark2 = millis()- screenrefreshtime ;
       
   if(fark2 > 350){


    if(reffreq > 5000 || reffreq < 400){
        return;
    }

    if(plugreffreq > 5000 || plugreffreq < 400){
        return;
    }

    
    /*FastRefClear();
    FastPlugClear();*/
    _tft.setTextSize(4);
     //##################################
    _tft.setCursor(63,52);
    _tft.setTextColor(TFT_WHITE);
    _tft.println(oldRefHz,3);
    _tft.setCursor(63,52);
    _tft.setTextColor(TFT_BLACK);
    _tft.println(reffreq,3);
    oldRefHz =reffreq;
    //##################################
    _tft.setCursor(63,82);
    _tft.setTextColor(TFT_WHITE);
    _tft.println(oldRefPa);
    _tft.setCursor(63,82);
    _tft.setTextColor(TFT_BLACK);
    _tft.println(CalcPa(reffreq));
    oldRefPa = CalcPa(reffreq);
    //##################################
    _tft.setCursor(223,52);
    _tft.setTextColor(TFT_WHITE);
    _tft.println(oldPlugHz,3);
    _tft.setCursor(223,52);
    _tft.setTextColor(TFT_BLACK);
    _tft.println(plugreffreq,3);
    oldPlugHz = plugreffreq;
    //#################################
    _tft.setCursor(223,82);
    _tft.setTextColor(TFT_WHITE);
    _tft.println(oldPlugPa);
    _tft.setCursor(223,82);
    _tft.setTextColor(TFT_BLACK);
    _tft.println(CalcPa(plugreffreq));
    oldPlugPa = CalcPa(plugreffreq);
    //################################
    _tft.setCursor(120,125);
    _tft.setTextColor(TFT_WHITE);
    _tft.println(oldDiff);
    _tft.setCursor(120,125);
    _tft.setTextColor(TFT_BLACK);
    _tft.println(PercentDiffCalculate(reffreq,plugreffreq));
    oldDiff = PercentDiffCalculate(reffreq,plugreffreq);
    //################################
    
    //################################
    screenrefreshtime = millis();
    
   }

       
  if(autotestrunning == 1){
        
       int fark = millis()- teststarttime ;
       
       if(fark > 1 && startact < 65535){
         
          teststarttime = millis();
          startact = startact + artisoran;
          ledcWrite(0, startact);
          //Serial.println(startact);
          
       }

      if(float(reffreq) < 503.100){
          teststarttime = 0;
          startact = startval;
          ledcWrite(0, 0);
          autotestrunning = 0;
          Buzzer(1000);
          firststarttest = 0;

          ShowTestResult();
          
          ResetStepArray();
          
          return;
      }

      if(firststarttest != 0){
            int saniye = (millis()-firststarttest) / 1000;
            if(saniye > 120){
              AutoTestStop(1);
              firststarttest = 0;
            }
      }
          
      SetCheckedPoints(reffreq,plugreffreq);
  }
  
}
void PreScreen::DrawTestboxes(){
   _tft.fillRect(0,152,319,22,TFT_WHITE);
  
   int startx = 8;
   int starty = 156;

   int boxwidth = 18;
   int boxheight = 18;
   
   int margin = 5;
  
 
  for (int i=0; i < totalStepCount; i++){
      
      int dynasx = (boxwidth * i) + (12 + (margin*i));
      _tft.drawRect(dynasx,starty,boxwidth,boxheight,TFT_BLACK);
   }
  
}
void PreScreen::DrawTestedBoxes(){
   int startx = 8;
   int starty = 156;

   int boxwidth = 18;
   int boxheight = 18;
   
   int margin = 5;
  
  for (int i = 0; i < totalStepCount; i++){
      //Serial.println(i);
      if(TestedSteps[i] == 0){
        int dynasx = (boxwidth * i) + (12 + (margin*i));
        _tft.drawRect(dynasx,starty,boxwidth,boxheight,TFT_BLACK);
      }

      if(TestedSteps[i] == 1){
        int dynasx = (boxwidth * i) + (12 + (margin*i));
        _tft.fillRect(dynasx+1,starty+1,boxwidth-1,boxheight-1,TFT_GREEN);
        //Serial.println("yesil ciz");
       
      } 
      if(TestedSteps[i] == 2){
        int dynasx = (boxwidth * i) + (12 + (margin*i));
        _tft.fillRect(dynasx+1,starty+1,boxwidth-1,boxheight-1,TFT_RED);
      } 
      
  }
  
  
}
void PreScreen::SetCheckedPoints(float refval,float plugval){
  
   float diff = 100 * (plugval-refval)/refval;

   

  int index = GetPointIndex(refval);

  if(index != -1){
      if(TestedSteps[index] != 2){
          if(diff <= percenttolerancemax && diff >= percenttolerancemin){
            if(TestedSteps[index] != 1){
              TestedSteps[index] = 1;
              //Serial.println("deger degisti");
              DrawTestedBoxes();
              if(abs(diff) > abs(TestedStepPercents[index])){
                TestedStepPercents[index] = diff;
              }
              
            }
            
            
          }else{
            if(TestedSteps[index] != 2){
              TestedSteps[index] = 2;
              //Serial.println("deger degisti negatif");
              DrawTestedBoxes();
            }

            if(abs(diff) > abs(TestedStepPercents[index])){
              TestedStepPercents[index] = diff;
            }
          }
          
      }
  }else{
    
    //Serial.println("index out of range");  
  }
}
int PreScreen::CalcPa(float hz){
  return int((-23.94449 * hz) + 14895.05248);
  /*
  if(float(hz) > 625.000 && float(hz) < 630.000){
    return 0;
  }

  if(float(hz) > 609.180 && float(hz) < 625.000){
    return 333;
  }

  if(float(hz) > 609.000 && float(hz) < 609.180){
    return 336;
  }

  if(float(hz) > 593.500 && float(hz) < 609.000){
    return 664;
  }

  if(float(hz) > 589.400 && float(hz) < 593.500){
    return 752;
  }

  if(float(hz) > 578.000 && float(hz) < 589.400){
    return 1000;
  }

  if(float(hz) > 563.700 && float(hz) < 578.000){
    return 1328;
  }

  if(float(hz) > 563.490 && float(hz) < 563.700){
    return 1333;
  }

  if(float(hz) > 556.300 && float(hz) < 563.490){
    return 1504;
  }

  if(float(hz) > 549.800 && float(hz) < 556.300){
    return 1664;
  }

  if(float(hz) > 536.800 && float(hz) < 549.800){
    return 2000;
  }

  if(float(hz) > 524.900 && float(hz) < 536.800){
    return 2328;
  }

  if(float(hz) > 513.600 && float(hz) < 524.900){
    return 2664;
  }

  if(float(hz) > 503.100 && float(hz) < 513.600){
    return 3000;
  }

  return 0;
  */
  
}

int PreScreen::GetPointIndex(float hz){
 

  if(float(hz) > 609.180 && float(hz) < 625.000){
    return 0;
  }

  if(float(hz) > 609.000 && float(hz) < 609.180){
    return 1;
  }

  if(float(hz) > 593.500 && float(hz) < 609.000){
    return 2;
  }

  if(float(hz) > 589.400 && float(hz) < 593.500){
    return 3;
  }

  if(float(hz) > 578.000 && float(hz) < 589.400){
    return 4;
  }

  if(float(hz) > 563.700 && float(hz) < 578.000){
    return 5;
  }

  if(float(hz) > 563.490 && float(hz) < 563.700){
    return 6;
  }

  if(float(hz) > 556.300 && float(hz) < 563.490){
    return 7;
  }

  if(float(hz) > 549.800 && float(hz) < 556.300){
    return 8;
  }

  if(float(hz) > 536.800 && float(hz) < 549.800){
    return 9;
  }

  if(float(hz) > 524.900 && float(hz) < 536.800){
    return 10;
  }

  if(float(hz) > 513.600 && float(hz) < 524.900){
    return 11;
  }

  if(float(hz) > 503.100 && float(hz) < 513.600){
    return 12;
  }  
  return -1;
}
void PreScreen::AutoTestStop(){
  firststarttest = 0;
  startact = startval;
  autotestrunning = 0;
  
  ResetStepArray();
  
  
  ledcWrite(0, 0);
  autotestrunning = 0;
  Buzzer(100);
  delay(50);
  Buzzer(100);
  delay(50);
  Buzzer(100);
          
}
void PreScreen::ShowTestResult(){
  isResultShowing = 1;
  _tft.fillRect(0,0,319,239,TFT_WHITE);
  _tft.setTextColor(TFT_BLACK);
  _tft.setTextSize(4);
  _tft.setCursor(5,0);
  _tft.println("Test Result");
  _tft.drawFastHLine(5,25,309,TFT_BLACK);

  int margintop = 2;
  int lineheight = 14;
  
  for (int i = 0; i < totalStepCount; i++){
    int resultleftmargin = 0;  
    int color = TFT_BLACK;
    String resulttext =  "";
    if(TestedSteps[i] == 1){
      color = TFT_GREEN;
      resultleftmargin = 278;
      resulttext = "Pass";
    }
    if(TestedSteps[i] == 2){
      color = TFT_RED;
      resultleftmargin = 280;
      resulttext = "Fail";
    }
    if(TestedSteps[i] == 0){
      color = TFT_BLACK;
      resultleftmargin = 290;
      resulttext = "----";
    }
    
    _tft.setTextColor(TFT_BLACK);
    _tft.setTextSize(3);
    _tft.setCursor(5,(lineheight * i) + (26 + (margintop*i)));
    _tft.println(TestedStepsStr[i]);

    _tft.setTextColor(color);
    _tft.setTextSize(3);
    _tft.setCursor(resultleftmargin,(lineheight * i) + (26 + (margintop*i)));
    _tft.println(resulttext);


    String percenttext;
    percenttext = F("%");
    percenttext+= String(TestedStepPercents[i],2);

    _tft.setTextColor(TFT_BLACK);
    _tft.setTextSize(3);
    _tft.setCursor(210,(lineheight * i) + (26 + (margintop*i)));
    _tft.println(percenttext);




    
    _tft.drawFastHLine(5,(lineheight * i) + (43 + (margintop*i)),309,TFT_BLACK);
  }

   

  /*
   * _tft.drawFastVLine(160,10,100,TFT_BLACK);     
   * _tft.drawFastHLine(10,40,300,TFT_BLACK);
   */
  
}
void PreScreen::ResetStepArray(){
   for (int i = 0; i < totalStepCount; i++){
    TestedSteps[i] = 0;
   }

   for (int i = 0; i < totalStepCount; i++){
    TestedStepPercents[i] = 0.0;
   }

}
void PreScreen::AutoTestStop(int buzzer){
  firststarttest = 0;
  startact = startval;
  autotestrunning = 0;

  ResetStepArray();
  
  
  ledcWrite(0, 0);
  autotestrunning = 0;

  if(buzzer == 1){
     Buzzer(100);
    delay(50);
    Buzzer(100);
    delay(50);
    Buzzer(100);
  }
 
          
} 
void PreScreen::AutoTestStart(){
  
  if(autotestrunning == 1){
    AutoTestStop(0);
    DrawTestboxes();
    delay(2000);
    autotestrunning=1;
    return;  
  }
  
  DrawTestboxes();
  
  startact = startval;
  autotestrunning = 1;
  teststarttime = millis();
  firststarttest = millis();
  Buzzer(200);
  delay(200);
  Buzzer(200);
  //Serial.println("Auto Test Started");
  /*
  while (reffreq > 503.100 && reffreq < 630.000) {
    ++startAct;
    
    ledcWrite(0, startAct);
  }

  Serial.println("bitti");
  */
}
void PreScreen::DrawResultArea(){
    _tft.fillRect(0,0,319,150,TFT_WHITE);
    _tft.drawFastVLine(160,10,100,TFT_BLACK);
    _tft.drawFastHLine(10,40,300,TFT_BLACK);
    _tft.setTextColor(TFT_BLACK);
    _tft.setTextSize(4);
    _tft.setCursor(28,10);
    _tft.println("Reference");

    _tft.setCursor(188,10);
    _tft.println("Plugged");

    //referans yerin hz yazısı
    _tft.setCursor(28,52);
    _tft.println("Hz:");
    
    //plug yerin hz yazısı
    _tft.setCursor(188,52);
    _tft.println("Hz:");

    //referans yerin pa yazısı
    _tft.setCursor(28,82);
    _tft.println("Pa:");

    //plug yerin pa yazısı
    _tft.setCursor(188,82);
    _tft.println("Pa:");

    //Diff Alanı
    _tft.setTextSize(3);
    _tft.setCursor(115,109);
    _tft.println("Difference");
    
}
void PreScreen::DrawMenu(int selected){

    if(selected == 1) AutoTest(TFT_ORANGE); else AutoTest(TFT_BLACK);
    //if(selected == 2) ManuelTest(TFT_ORANGE); else ManuelTest(TFT_BLACK);
    
}
void PreScreen::AutoTest(int bordercolor){

  //x,y,width,height,color
  
  _tft.drawRect(10,188,299,40,bordercolor);
  _tft.drawRect(11,189,297,38,bordercolor);
  _tft.drawRect(12,190,295,36,bordercolor);
  
  _tft.setTextColor(bordercolor);
  _tft.setTextSize(4);
  _tft.setCursor(110,195);
  _tft.println("Auto Test");
}
 
String PreScreen::PercentDiffCalculate(float valref,float valplug){
    // formul bu
    //100 x (B-A)/A

    
    float diff = 100 * (valplug-valref)/valref;
    
    String statustext;
    statustext = F("%");
    statustext+= String(diff,2);
    //Serial.println(valplug);
    //Serial.println(valref);
    return statustext;
    
    

}
String PreScreen::ButtonPressed(String button){
  
  //auto test seçiliyken
  if(customitem == 1){
    if(button=="enter"){
      if(isResultShowing == 1){
          isResultShowing = 0;
          init();
          return "";
      }
      AutoTestStart();
    }
    if(button=="cancel"){
      if(isResultShowing == 1){
          isResultShowing = 0;
          init();
          return "";
      }
      if(autotestrunning == 1){
        ShowTestResult();
        AutoTestStop(1);
        
        return "";
      }else{
        return "mainmenu";  
      }
        
        
    }
    /*if(button=="right"){
      customitem = 2;
      DrawMenu(2);
    }
    */
  }
  //manuel test seçiliyken
  if(customitem == 2){
    if(button=="enter"){
      
    }
    if(button=="cancel"){
        return "mainmenu";
    }
    if(button=="left"){
      customitem = 1;
      DrawMenu(1);
    }
  }
  return "prescreen";
  
}
void PreScreen::ManuelTest(int bordercolor){

  //x,y,width,height,color
  
  _tft.drawRect(165,188,145,40,bordercolor);
  _tft.drawRect(166,189,143,38,bordercolor);
  _tft.drawRect(167,190,141,36,bordercolor);
  
  _tft.setTextColor(bordercolor);
  _tft.setTextSize(4);
  _tft.setCursor(173,195);
  _tft.println("Manuel Test");
}

void PreScreen::FastRefClear(){
  //_tft.fillRect(63,55,88,50,TFT_WHITE);
   for (int i=55; i <= 105; i++){
      _tft.drawFastHLine(63,i,88,TFT_WHITE);
   }
}

void PreScreen::FastPlugClear(){
  //_tft.fillRect(63,55,88,50,TFT_WHITE);
   for (int i=55; i <= 105; i++){
      _tft.drawFastHLine(223,i,88,TFT_WHITE);
   }
}
